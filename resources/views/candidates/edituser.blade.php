@extends('layouts.app')

@section('title', 'Edit user')

@section('content')
       
       <h1>Edit user</h1>
       <form method = "post" action = "{{action('CandidatesController@updateUser',$user->id)}}">
        @csrf
        @METHOD('PATCH')
        <div class="form-group">
            <label for = "name">Candidate name</label>
            <input type = "text" class="form-control" name = "name" value = {{$user->name}}>
        </div>     
        <div class="form-group">
            <label for = "email">Candidate email</label>
            <input type = "text" class="form-control" name = "email" value = {{$user->email}}>
        </div> 
        <div>
            <input type = "submit" name = "submit" value = "Update user">
        </div>                       
        </form>    
    </body>
</html>
@endsection

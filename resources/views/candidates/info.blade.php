@extends('layouts.app')

@section('title', 'candidate info')

@section('content')      

       <h1>Candidate details</h1>
       
         
    <table class = "table table-dark">
    <th>id</th><th>Name</th><th>Age</th></th><th>Email</th><th>Owner</th><th>Status</th><th>Created</th><th>Updated</th>
        
        
        <tr>
        <td>{{$candidate->id}}</td>
        <td>{{$candidate->name}}</td>
        <td>{{$candidate->age}}</td>
        <td>{{$candidate->email}}</td>
        <td>@if(isset($candidate->user_id))
                {{$candidate->owner->name}}  
            @else
                Assign owner
            @endif</td>
        
        @if (null != App\Status::next($candidate->status_id)) 
        <form method="POST" action="{{ route('candidates.changestatusfromcandidate') }}">
                    @csrf  
                    <div class="form-group row">
                    <label for="department_id" class="col-md-4 col-form-label text-md-right">Move to status</label>
                    <div class="col-md-6">
                        <select class="form-control" name="status_id">                                                                         
                          @foreach (App\Status::next($candidate->status_id) as $status)
                          <option value="{{ $status->id }}"> 
                              {{ $status->name }} 
                          </option>
                          @endforeach    
                        </select>
                    </div>
                    <input name="id" type="hidden" value = {{$candidate->id}} >
                    <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Change status
                                </button>
                            </div>
                    </div>                    
                </form> 
                </div>
        @endif

@endsection

 
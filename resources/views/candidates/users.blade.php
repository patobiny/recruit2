@extends('layouts.app')

@section('title', 'Users info')

@section('content')      
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
@if(Session::has('notallowed2'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed2')}}
</div>
@endif
       <h1>Users details</h1>
       
         
    <table class = "table table-light">
    <th>id</th><th>Name</th><th>Email</th><th>Department</th><th>Created</th>
    @foreach($users as $user)
    @if($user->ronaldinho==1)
    <tr style="background-color:#98FB98">
    @else  
        <tr>
        @endif
        <td>{{$user->id}}</td>
        <td>{{$user->name}}</td>
        <td>{{$user->email}}</td>
        <td>{{$user->department->name}}</td>
        <td>{{$user->created_at}}</td>
        <td>
                <a href = "{{route('users.edit',$user->id)}}">Edit</a>
            </td> 
            <td>
            <a href = "{{route('users.delete',$user->id)}}">Delete</a> 
            </td>
            <td>
            @if(Gate::allows('assign-user'))
            @if($user->ronaldinho!=1)
            <a href = "{{route('users.ronaldinho',$user->id)}}">Ronaldinho</a>
            
            @endif 
            @endif
            </td>            
        </tr>
   
    @endforeach
@endsection

 
<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CustomersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            [
                'name' => 'aaa',
                'email'=>'aaa@aaa.com',
                'phone'=>random_int(0,10),
                'user_id'=>1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'aaa',
                'email'=>'aaa@aaa.com',
                'phone'=>random_int(0,10),
                'user_id'=>2,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],        
            [
                'name' => 'ccc',
                'email'=>'ccc@ccc.com',
                'phone'=>random_int(0,10),
                'user_id'=>3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
                            
            ]);  
}
}
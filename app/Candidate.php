<?php

namespace App;
/****/ 
use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $fillable = ['name','age','email'];

    public function owner(){
        return $this->belongsTo('App\User','user_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }    
    
 

}

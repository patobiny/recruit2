<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Candidate;
use App\User;
use App\Status;
use App\Department;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();
        //$candidate->name = $request->name; 
        //$candidate->email = $request->email;
        $us = $user->create($request->all());
        $us->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /*********
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function edit($uid)
    {
        if(Gate::allows('add-user'))
        $user = User::findOrFail($uid);
        return view('candidates.users', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uid)
    {
        
       $user = User::findOrFail($uid);
       $user->update($request->all());
       
       return view('candidates.edituser',compact('user'));  
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($uid)
    {
        if(Gate::allows('add-user')){
        $user = User::findOrFail($uid);
        $user->delete(); 
        return back();
    }else{
            Session::flash('notallowed2', 'You are not allowed to delete users because you are not an admin');
        }return back();
    
    }
    public function changeRon($uid)
    {
        if(Gate::allows('assign-user')){
       $user = User::findOrFail($uid);
       $user->ronaldinho=1;
       $user->save();
       $users=User::all();
       return view('candidates.users',compact('users'));  
        }else{
            Session::flash('notallowed3');
            return back();
        }
    
    }


}
